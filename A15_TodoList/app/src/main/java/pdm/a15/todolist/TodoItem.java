package pdm.a15.todolist;

import java.util.GregorianCalendar;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import pdm.a15.todolist.contentprovider.TodoListContentProviderContract;


/**
 * A todo item.
 *
 * @author Mauro Ferrari
 */
public class TodoItem {

    private static String TAG = "TodoItem";
    private Context context; // we need this to access resources from this class
    private long _ID;
    private String task;
    private GregorianCalendar creationDate;
    private GregorianCalendar dueDate;
    private boolean isDueDateRequired;
    private Uri imageUri;

    /**
     * Creates a TodoItem with idx = -1
     *
     * @param context           the context of the application where the TodoItem is created.
     * @param task
     * @param creationDate
     * @param isDueDateRequired
     * @param dueDate
     * @param imageUri
     */
    public TodoItem(Context context, String task, GregorianCalendar creationDate,
                    boolean isDueDateRequired, GregorianCalendar dueDate, Uri imageUri) {
        this(context, -1, task, creationDate, isDueDateRequired, dueDate, imageUri);
    }

    /**
     * Creates a todoItem with full details.
     *
     * @param context           the context of the application where the TodoItem is created.
     * @param context
     * @param idx
     * @param task
     * @param creationDate
     * @param isDueDateRequired
     * @param dueDate
     * @param imageUri
     */
    public TodoItem(Context context, long idx, String task,
                    GregorianCalendar creationDate, boolean isDueDateRequired,
                    GregorianCalendar dueDate, Uri imageUri) {
        super();
        this.context = context.getApplicationContext(); // get application context to avoid memory leaks
        this._ID = idx;
        this.task = task;
        this.creationDate = creationDate;
        this.isDueDateRequired = isDueDateRequired;
        this.dueDate = dueDate;
        if (this.isDueDateRequired && this.dueDate == null)
            throw new ImplementationError("Due date is true, dueDate cannot be null");
        this.imageUri = imageUri;
    }

    /**
     * Reconstruct a TodoItem from a Bundle.
     *
     * @param bundle
     */
    public static TodoItem buildFrom(Context context, Bundle bundle) {
        String task = bundle.getString(NVP_KEY.TASK);
        GregorianCalendar creationDate = ((GregorianCalendar) GregorianCalendar.getInstance());
        creationDate.setTimeInMillis(bundle.getLong(NVP_KEY.CREATION_DATE));
        boolean isDueDateRequired = bundle.getBoolean(NVP_KEY.IS_DUE_DATE_REQUIRED);
        GregorianCalendar dueDate = null;
        if (isDueDateRequired) {
            dueDate = ((GregorianCalendar) GregorianCalendar.getInstance());
            dueDate.setTimeInMillis(bundle.getLong(NVP_KEY.DUE_DATE));
        }
        Uri imageUri = null;
        if (bundle.getString(NVP_KEY.IMAGE_URI) != null)
            imageUri = Uri.parse(bundle.getString(NVP_KEY.IMAGE_URI));
        return new TodoItem(context, task, creationDate, isDueDateRequired, dueDate, imageUri);
    }


    public static TodoItem buildFromContentProviderCursor(Context context, Cursor cursor) {
        long idx = cursor.getLong(cursor.getColumnIndex(TodoListContentProviderContract.TodoItems
                ._ID));
        String task = cursor.getString(cursor.getColumnIndex(TodoListContentProviderContract
                .TodoItems
                .COLUMN_NAME_TASK));
        GregorianCalendar creationDate = new GregorianCalendar();
        creationDate.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(TodoListContentProviderContract.TodoItems
                .COLUMN_NAME_CREATION_DATE)));
        boolean isDueDateDefined = cursor.getInt(cursor.getColumnIndex
                (TodoListContentProviderContract.TodoItems.COLUMN_NAME_IS_DUEDATE_REQUIRED)) == 1;
        GregorianCalendar dueDate = null;
        if (isDueDateDefined) {
            dueDate = new GregorianCalendar();
            dueDate.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(
                    TodoListContentProviderContract.TodoItems
                            .COLUMN_NAME_DUE_DATE)));
        }

        // get the image Uri
        String uriString = cursor.getString(cursor
                .getColumnIndex(TodoListContentProviderContract.TodoItems
                        .COLUMN_NAME_URI_IMAGE));
        Uri uriImage = null;
        if (uriString != null)
            uriImage = Uri.parse(uriString);


        // build and return the TodoItem instance
        return new TodoItem(context, idx, task, creationDate, isDueDateDefined,
                dueDate, uriImage);
    }

    public String getTask() {
        return this.task;
    }

    public Uri getImageUri() {
        return this.imageUri;
    }

    public Bitmap getImage() {

        if (imageUri != null)
            return BitmapFactory.decodeFile(imageUri.getPath());
        else
            return null;
    }

    public GregorianCalendar getCreationDate() {
        return this.creationDate;
    }

    public GregorianCalendar getDueDate() {
        return this.dueDate;
    }

    public long getID() {
        return this._ID;
    }

    public void setID(long id) {
        this._ID = id;
    }

    public boolean isDueDateRequired() {
        return isDueDateRequired;
    }

    /**
     * @return the content values representing this item.
     */
    public ContentValues getAsContentValue() {
        ContentValues cv = new ContentValues();
        cv.put(TodoListContentProviderContract.TodoItems.COLUMN_NAME_TASK, this.task);
        cv.put(TodoListContentProviderContract.TodoItems.COLUMN_NAME_CREATION_DATE, String.valueOf(this.creationDate
                .getTimeInMillis()));
        cv.put(TodoListContentProviderContract.TodoItems.COLUMN_NAME_IS_DUEDATE_REQUIRED, this.isDueDateRequired ? 1
                : 0);
        if (this.isDueDateRequired)
            cv.put(TodoListContentProviderContract.TodoItems.COLUMN_NAME_DUE_DATE, String.valueOf(dueDate
                    .getTimeInMillis()));
        if (this.imageUri != null)
            cv.put(TodoListContentProviderContract.TodoItems.COLUMN_NAME_URI_IMAGE, this.imageUri.toString());
        return cv;
    }

    /**
     * @return the bundle representing this TodoItem
     */
    public Bundle getAsBundle() {
        Bundle result = new Bundle();
        result.putLong(NVP_KEY.ID, _ID);
        result.putString(NVP_KEY.TASK, task);
        result.putLong(NVP_KEY.CREATION_DATE, creationDate.getTimeInMillis());
        result.putBoolean(NVP_KEY.IS_DUE_DATE_REQUIRED, isDueDateRequired);
        if (isDueDateRequired)
            result.putLong(NVP_KEY.DUE_DATE, creationDate.getTimeInMillis());
        if (imageUri != null)
            result.putString(NVP_KEY.IMAGE_URI, imageUri.toString());
        return result;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.task);
        sb.append("\n");
        sb.append(String.format(context.getString(R.string.todo_item_to_string_created_on), this.creationDate.getTime()));
        sb.append("\n");
        if (isDueDateRequired) {
            sb.append(String.format(context.getString(R.string.todo_item_to_string_due_date), this.dueDate.getTime()));
            sb.append("\n");
        }
        sb.append(String.format(context.getString(R.string.todo_item_to_string_idx), this._ID));
        return sb.toString();
    }

    /* Names of the keys NVP (name/value pairs) representation equals to colunm names*/
    public static class NVP_KEY {
        public static String ID = TodoListContentProviderContract.TodoItems._ID;
        public static String TASK = TodoListContentProviderContract.TodoItems.COLUMN_NAME_TASK;
        public static String CREATION_DATE = TodoListContentProviderContract.TodoItems.COLUMN_NAME_CREATION_DATE;
        public static String DUE_DATE = TodoListContentProviderContract.TodoItems.COLUMN_NAME_DUE_DATE;
        public static String IS_DUE_DATE_REQUIRED = TodoListContentProviderContract.TodoItems
                .COLUMN_NAME_IS_DUEDATE_REQUIRED;
        public static String IMAGE_URI = TodoListContentProviderContract.TodoItems.COLUMN_NAME_URI_IMAGE;
    }

}
