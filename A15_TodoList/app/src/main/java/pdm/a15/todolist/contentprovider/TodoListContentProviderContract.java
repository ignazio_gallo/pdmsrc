package pdm.a15.todolist.contentprovider;

import android.net.Uri;
import android.provider.BaseColumns;

public class TodoListContentProviderContract {

	public static final String AUTHORITY = "content://pdm.a15.todolist.provider/";

	public static Uri CONTENT_URI = Uri.parse("content://pdm.a15.todolist.provider/todoitems");

    public static class TodoItems implements BaseColumns {
        // These names corresponds to DB table names
        public static final String TABLE_NAME = "todoItems";
		public static final String COLUMN_NAME_TASK = "task";
		public static final String COLUMN_NAME_IS_DUEDATE_REQUIRED = "duedate_required";
		public static final String COLUMN_NAME_CREATION_DATE = "creation_date";
		public static final String COLUMN_NAME_DUE_DATE = "due_date";
		public static final String COLUMN_NAME_URI_IMAGE = "uri_image";
        // This is a column added by the content provider
        public static final String COLUMN_NAME_IMAGE = "image";

	}

}
