package pdm.a15.todolist.contentprovider;

import android.database.Cursor;
import android.database.CursorWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import java.io.ByteArrayOutputStream;
import java.util.Arrays;

import pdm.a15.todolist.database.DBContract;

/**
 * Created by ferram on 28/04/16.
 */
public class CustomCursor extends CursorWrapper {
    private static final String TAG = "CustomCursor";

    private int COLUMN_IMAGE_INDEX;
    private int COLUMN_COUNT;


    public CustomCursor(Cursor cursor) {
        super(cursor);
        // we explicitly treat the extra column
        this.COLUMN_IMAGE_INDEX = cursor.getColumnCount();
        this.COLUMN_COUNT = cursor.getColumnCount() + 1;


        // it add the cursor an extra (a Bundle) containing the NVP pair (_id,byte[]) for every
        // id having a bytmap
        Bundle bundle = new Bundle();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) { // for every row in the cursor
            // get the id
            long id = cursor.getLong(cursor.getColumnIndex(DBContract.TodoItems._ID));
            // get the uriString for the id element
            String uriString = cursor.getString(cursor.getColumnIndex(DBContract.TodoItems
                    .COLUMN_NAME_URI_IMAGE));
            if (uriString != null) { // if there is an image
                // get the image
                Uri uriImage = Uri.parse(uriString);
                Bitmap image = BitmapFactory.decodeFile(uriImage.getPath());
                // create the stream
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                // put the array of bytes in the bundle with key _id
                bundle.putByteArray("_" + id, stream.toByteArray());
            }
            cursor.moveToNext();
        }
        // add the bundle to this cursor
        cursor.setExtras(bundle);
    }

    @Override
    public int getColumnIndex(String columnName) {
        // treat the new column name
        if (columnName == TodoListContentProviderContract.TodoItems.COLUMN_NAME_IMAGE)
            return COLUMN_IMAGE_INDEX;
        else
            return super.getColumnIndex(columnName);
    }

    @Override
    public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
        // treat the new column name
        if (columnName == TodoListContentProviderContract.TodoItems.COLUMN_NAME_IMAGE)
            return COLUMN_IMAGE_INDEX;
        else
            return super.getColumnIndexOrThrow(columnName);
    }

    @Override
    public int getColumnCount() {
        return COLUMN_COUNT;
    }

    @Override
    public String[] getColumnNames() {
        String[] result = Arrays.copyOf(super.getColumnNames(), COLUMN_COUNT);
        result[COLUMN_COUNT - 1] = TodoListContentProviderContract.TodoItems.COLUMN_NAME_IMAGE;
        return result;
    }

    @Override
    public byte[] getBlob(int columnIndex) {
        // explicitly treat the case where COLUMN_NAME_IMAGE is required
        if (columnIndex == COLUMN_IMAGE_INDEX) {
            // get the row id
            long id = this.getLong(this.getColumnIndex(TodoListContentProviderContract.TodoItems._ID));
            // return the corresponding array of bytes from extras
            return this.getExtras().getByteArray("_" + id);
        } else
            return super.getBlob(columnIndex);
    }

    @Override
    public int getType(int columnIndex) {
        // treat the type of the new column
        if (columnIndex == COLUMN_IMAGE_INDEX)
            return Cursor.FIELD_TYPE_BLOB;
        else
            return super.getType(columnIndex);
    }


}
