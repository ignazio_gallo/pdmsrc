package pdm.a11.imagedownload;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.Toolbar;

public class ImageDownloadActivity extends Activity {

	private static String TAG = "ImageDownloadActivity";

	// Image URL
	private static final String URL_IMG = "http://imgsrc.hubblesite.org/hu/db/images/hs-2006-10-a-2560x1024_wallpaper.jpg";

	private ImageView imageView;
	private Button buttonDownload;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        // set content view
        setContentView(R.layout.main);
        // set the action bar
        Toolbar tb = (Toolbar) findViewById(R.id.main_activity_toolbar);
        this.setActionBar(tb);

		imageView = (ImageView) findViewById(R.id.img_view);

		// Download button
		buttonDownload = (Button) findViewById(R.id.download_button);
		buttonDownload.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ImageDownloader imageDownloader = new ImageDownloader();
				imageDownloader.execute(URL_IMG);
			}
		});
	}

	/**
	 * Background AsyncTask per scaricare il file
	 */
	class ImageDownloader extends AsyncTask<String, Void, Bitmap> {

		// executed on the UI-thread
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			buttonDownload.setEnabled(false);
		}

		// background worker
		@Override
		protected Bitmap doInBackground(String... imageURL) {
			Bitmap result = loadImageFromNetwork(imageURL[0]);
			if (result == null)
				cancel(true);
			return result;
		}

		public Bitmap loadImageFromNetwork(String imageURL) {
			Bitmap result = null;
			InputStream is = null;
			BufferedInputStream bis = null;
			try {
				URL url = new URL(imageURL);
				Log.d(TAG, "download begining");
				Log.d(TAG, "download url:" + url);
				/* Open a connection to that URL. */
				URLConnection ucon = url.openConnection();

				/*
				 * Define InputStreams to read from the URLConnection.
				 */
				is = ucon.getInputStream();
				bis = new BufferedInputStream(is);

				result = BitmapFactory.decodeStream(bis);
				Log.d("TAG", "download end");
			} catch (MalformedURLException e) {
				Log.e(TAG, "Bad ad URL", e);
				result = null;
			} catch (IOException e) {
				Log.d("ImageManager", "Error: " + e);
				result = null;
			} finally {
				try {
					if (is != null)
						is.close();
					if (bis != null)
						bis.close();
				} catch (IOException e) {
					Log.w(TAG, "Error closing stream.");
					result = null;
				}
			}
			return result;
		}

		// executed on the UI-thread
		@Override
		protected void onPostExecute(Bitmap image) {
			if (image != null)
				imageView.setImageBitmap(image);
			buttonDownload.setEnabled(true);
		}

		@Override
		protected void onCancelled() {
			Log.i(TAG, "Download cancelled.");
			Toast.makeText(ImageDownloadActivity.this, "Download Cancelled", Toast.LENGTH_LONG).show();
			buttonDownload.setEnabled(true);
		}

	}
}