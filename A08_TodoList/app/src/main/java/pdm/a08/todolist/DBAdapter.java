package pdm.a08.todolist;

import java.io.ByteArrayOutputStream;

import android.content.ContentValues;
import android.content.Context;
import android.database.*;
import android.database.sqlite.*;
import android.graphics.Bitmap;
import android.util.Log;

/**
 * Adapter for the DB
 */
class DBAdapter {

    // the tag used in LogCat messages
    private static String TAG = "DBAdapter";

    private static DBAdapter sInstance;  //singleton instance

    // ADAPTER STATE
    private SQLiteDatabase db; // reference to the DB
    private DBOpenHelper dbHelper; // reference to the OpenHelper

    public static synchronized DBAdapter getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        if (sInstance == null)
            sInstance = new DBAdapter(context.getApplicationContext());

        return sInstance;
    }

    private DBAdapter(Context context) {
        this.dbHelper = DBOpenHelper.getInstance(context);
    }


    /**
     * Open the DB in write mode. If the DB cannot be opened in write mode, the
     * method throws an exception to signal the failure
     *
     * @return this (self reference, allowing this to be chained in an
     * initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public DBAdapter open() throws SQLException {
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLiteException e) {
            Log.e(TAG, e.getMessage());
            throw e;
        }
        return this;
    }


    /**
     * Close the DB.
     */
    public void close() {
        db.close();
    }

    /**
     * Add the specified element to the DB.
     *
     * @param item
     * @return @return rowId or -1 if failed.
     */
    public long insert(TodoItem item) {
        Log.w(TAG, "Adding todoItem to the database." + item.toString());
        ContentValues cv = item.getAsContentValue();
        long idx = db.insert(DBContract.TodoItems.TABLE_NAME, null, item.getAsContentValue());
        // we set here the idx
        item.setID(idx);
        Log.w(TAG, "Added value idx: " + idx);
        return idx;
    }

    /**
     * Delete the TodoItem with the the given idx.
     *
     * @param idx
     * @return true if deleted, false otherwise.
     */
    public boolean deleteTodoItem(long idx) {
        return db.delete(DBContract.TodoItems.TABLE_NAME, DBContract.TodoItems._ID + "=" + idx, null) == 1;
    }

    /**
     * Delete the specified TodoItem from the DB.
     *
     * @param item the item to delete.
     * @return true if deleted, false otherwise.
     */
    public boolean delete(TodoItem item) {
        Log.v(TAG, "Removing TodoItem with idx: " + item.getID());
        return db.delete(DBContract.TodoItems.TABLE_NAME,
                DBContract.TodoItems._ID + "=" + item.getID(),
                null) == 1;
    }

    /**
     * Return a Cursor over the list of all items in the database
     *
     * @return Cursor over all todoItems
     */
    public Cursor getAllEntries() {
        return db.query(DBContract.TodoItems.TABLE_NAME, null, null, null, null, null, null);
    }


    public void deleteTodoItemsTable() {
        db.delete(DBContract.TodoItems.TABLE_NAME, null, null);
        db.execSQL("delete from sqlite_sequence where name='" + DBContract.TodoItems.TABLE_NAME + "';");
    }

}