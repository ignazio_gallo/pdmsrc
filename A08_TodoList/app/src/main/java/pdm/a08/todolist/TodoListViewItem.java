package pdm.a08.todolist;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

public class TodoListViewItem extends TextView {

    // background color
    private int paperColor;

    /*
     * The Paint class holds the style and color information about how to draw
     * geometries, text and bitmaps. linePaint store the style and color
     * information about the line
     */
    private Paint linePaint;

    /*
     * Simple constructor to use when creating a view from code.
     */
    public TodoListViewItem(Context context) {
        super(context);
        init(context);
    }

    /*
     * Constructor that is called when inflating a view from XML.
     */
    public TodoListViewItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    /*
     * Perform inflation from XML and apply a class-specific base style.
     */
    public TodoListViewItem(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        // get the background color and the line color from resources
        paperColor = context.getColor(R.color.notepad_paper);

        // set the style for the line between date and task
        linePaint = new Paint();
        linePaint.setColor(context.getColor(R.color.notepad_lines));
        linePaint.setStrokeWidth(4);
    }

    /*
     * The Canvas class holds the "draw" calls.
     * To draw something, you need a basic components: a Bitmap to hold the
     * pixels, a Canvas to host the draw calls (writing into the bitmap), a
     * drawing primitive (e.g. Rect, Path, text, Bitmap), and a paint (to describe
     * the colors and styles for the drawing).
     */
    @Override
    public void onDraw(Canvas canvas) { // here we "write" on the canvas prepared by the superclass
        // set the color of the canvas
        canvas.drawColor(paperColor);
        // draw a line under the first text line
        float ruleLineY = getBaseline() + (getBaseline() * 0.1f);
        float ruleLineXstart = getPaddingLeft();
        float ruleLineXstop = getMeasuredWidth() - getPaddingRight();
        canvas.drawLine(ruleLineXstart, ruleLineY, ruleLineXstop, ruleLineY, linePaint);
        this.setLineSpacing(10, 1);

        // let us draw by calling the method of the super class
        super.onDraw(canvas);
    }

}
