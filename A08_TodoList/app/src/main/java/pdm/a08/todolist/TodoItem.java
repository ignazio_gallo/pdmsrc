package pdm.a08.todolist;

import java.io.ByteArrayOutputStream;
import java.util.GregorianCalendar;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;


/**
 * A todo item.
 *
 * @author Mauro Ferrari
 */
public class TodoItem {

    private static String TAG = "TodoItem";
    private Context context; // we need this to access resources from this class
    private long _ID;
    private String task;
    private GregorianCalendar creationDate;
    private GregorianCalendar dueDate;
    private boolean isDueDateRequired;
    private Bitmap image;

    /**
     * Creates an instance of a todo item.
     *
     * @param task              the string decribing the task.
     * @param creationDate      the creation date.
     * @param dueDate           the duedate
     * @param isDueDateRequired
     */
    public TodoItem(Context context, String task, GregorianCalendar creationDate,
                    boolean isDueDateRequired, GregorianCalendar dueDate, Bitmap image) {
        this(context, -1, task, creationDate, isDueDateRequired, dueDate, image);
    }

    /**
     * Creates an instance with all the details.
     *
     * @param idx               the todoitem index
     * @param task
     * @param creationDate
     * @param dueDate
     * @param isDueDateRequired
     */
    public TodoItem(Context context, long idx, String task,
                    GregorianCalendar creationDate, boolean isDueDateRequired,
                    GregorianCalendar dueDate, Bitmap image) {
        super();
        this.context = context.getApplicationContext(); // get application context to avoid memory leaks
        this._ID = idx;
        this.task = task;
        this.creationDate = creationDate;
        this.isDueDateRequired = isDueDateRequired;
        this.dueDate = dueDate;
        if (this.isDueDateRequired && this.dueDate == null)
            throw new ImplementationError("Due date is true, dueDate cannot be null");
        this.image = image;
    }

    /**
     * Reconstruct a TodoItem from a Bundle.
     *
     * @param bundle
     */
    public static TodoItem buildFrom(Context context, Bundle bundle) {
        String task = bundle.getString(NVP_KEY.TASK);
        GregorianCalendar creationDate = ((GregorianCalendar) GregorianCalendar.getInstance());
        creationDate.setTimeInMillis(bundle.getLong(NVP_KEY.CREATION_DATE));
        boolean isDueDateRequired = bundle.getBoolean(NVP_KEY.IS_DUE_DATE_REQUIRED);
        GregorianCalendar dueDate = null;
        if (isDueDateRequired) {
            dueDate = ((GregorianCalendar) GregorianCalendar.getInstance());
            dueDate.setTimeInMillis(bundle.getLong(NVP_KEY.DUE_DATE));
        }
        Bitmap image = (Bitmap) bundle.getParcelable(NVP_KEY.IMAGE);
        return new TodoItem(context, task, creationDate, isDueDateRequired, dueDate,image);
    }

    public String getTask() {
        return this.task;
    }

    public Bitmap getImage() {
        return this.image;
    }

    public GregorianCalendar getCreationDate() {
        return this.creationDate;
    }

    public GregorianCalendar getDueDate() {
        return this.dueDate;
    }

    public long getID() {
        return this._ID;
    }

    public void setID(long id) {
        this._ID = id;
    }

    public boolean isDueDateRequired() {
        return isDueDateRequired;
    }

    /**
     * @return the content values representing this item.
     */
    public ContentValues getAsContentValue() {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.TodoItems.COLUMN_NAME_TASK, this.task);
        cv.put(DBContract.TodoItems.COLUMN_NAME_CREATION_DATE, String.valueOf(this.creationDate.getTimeInMillis()));
        cv.put(DBContract.TodoItems.COLUMN_NAME_IS_DUEDATE_REQUIRED, this.isDueDateRequired ? 1
                : 0);
        if (this.isDueDateRequired)
            cv.put(DBContract.TodoItems.COLUMN_NAME_DUE_DATE, String.valueOf(dueDate.getTimeInMillis()));
        if (image != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] imageBytes = stream.toByteArray();

            cv.put(DBContract.TodoItems.COLUMN_NAME_IMAGE, imageBytes);
        }
        return cv;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.task);
        sb.append("\n");
        sb.append(String.format(context.getString(R.string.todo_item_to_string_created_on), this.creationDate.getTime()));
        sb.append("\n");
        if (isDueDateRequired) {
            sb.append(String.format(context.getString(R.string.todo_item_to_string_due_date), this.dueDate.getTime()));
            sb.append("\n");
        }
        sb.append(String.format(context.getString(R.string.todo_item_to_string_idx), this._ID));
        return sb.toString();
    }

    /* Names of the keys NVP (name/value pairs) representation equals to colunm names*/
    public static class NVP_KEY {
        public static String TASK = DBContract.TodoItems.COLUMN_NAME_TASK;
        public static String CREATION_DATE = DBContract.TodoItems.COLUMN_NAME_CREATION_DATE;
        public static String DUE_DATE = DBContract.TodoItems.COLUMN_NAME_DUE_DATE;
        public static String IS_DUE_DATE_REQUIRED = DBContract.TodoItems.COLUMN_NAME_IS_DUEDATE_REQUIRED;
        public static String IMAGE = DBContract.TodoItems.COLUMN_NAME_IMAGE;
    }

}
