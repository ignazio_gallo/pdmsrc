package pdm.a02.buttonexample;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


/**
 * Default activity managing 3 buttons.
 */
public class MainActivity extends Activity {

	private static final String TAG = "MainActivity";
	private int counter = 0;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        Log.d(TAG,"onCreate()");

		setContentView(R.layout.activity_main);

		// we get the view references
		final TextView output = (TextView) findViewById(R.id.output);
		final Button button_plus = (Button) findViewById(R.id.button_plus);
		final Button button_minus = (Button) findViewById(R.id.button_minus);
		final Button button_reset = (Button) findViewById(R.id.button_reset);

		// get counter string
		final String counterFormatString = getResources().getString(R.string.counter);

		// listener for button_plus
    	button_plus.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				counter++;
				Log.d(TAG,"Click on +1 button, counter=" +counter);
				output.setText(String.format(counterFormatString,counter));
			}
		});

		// listener for button_minus
		button_minus.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				counter--;
				Log.d(TAG,"Click on +1 button, counter=" +counter);
				output.setText(String.format(counterFormatString,counter));
			}
		});

		// listener for button_reset
		button_reset.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				counter = 0;
				Log.d(TAG,"Click on reset button, counter=" +counter);
				output.setText(String.format(counterFormatString,counter));
			}
		});
	}
}