package pdm.a10.imagedownload;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toolbar;

public class ImageDownloadActivity extends Activity {

    private static final String URL_IMG = "http://imgsrc.hubblesite.org/hu/db/images/hs-2006-10-a-2560x1024_wallpaper.jpg";
    private final String TAG = "ImageDownloadActivity";

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set content view
        setContentView(R.layout.main);
        // set the action bar
        Toolbar tb = (Toolbar) findViewById(R.id.main_activity_toolbar);
        this.setActionBar(tb);

        final Button downloadButton = (Button) findViewById(R.id.download_button);
        final ImageView imgView = (ImageView) findViewById(R.id.img_view);


        //		// SOLUZIONE 1 - ERRATA: operazione lunga in UI-thread.
        //				downloadButton.setOnClickListener(new View.OnClickListener() {
        //
        //					@Override
        //					public void onClick(View v) {
        //						Bitmap b = loadImageFromNetwork(URL_IMG);
        //						imgView.setImageBitmap(b);
        //
        //					}
        //				});

        //SOLUZIONE 2 - ERRATA: accesso all'ImageView da un thread diverso
        //		downloadButton.setOnClickListener(new View.OnClickListener() {
        //
        //			@Override
        //			public void onClick(View v) {
        //				new Thread(new Runnable() {
        //
        //					public void run() {
        //						Bitmap b = loadImageFromNetwork(URL_IMG);
        //						imgView.setImageBitmap(b);
        //					}
        //				}).start();
        //			}
        //		});

        //        SOLUZIONE 3: CORRETTA
        downloadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {

                    public void run() {
                        final Bitmap bitmap = loadImageFromNetwork(URL_IMG);
                        imgView.post(new Runnable() {

                            public void run() {
                                imgView.setImageBitmap(bitmap);
                            }
                        });
                    }
                }).start();
            }
        });


        //        // SOLUZIONE 4: CORRETTA
        //        downloadButton.setOnClickListener(new View.OnClickListener() {
        //
        //            @Override
        //            public void onClick(View v) {
        //                new Thread(new Runnable() {
        //
        //                    public void run() {
        //                        final Bitmap bitmap = loadImageFromNetwork(URL_IMG);
        //                        ImageDownloadActivity.this.runOnUiThread(new Runnable() {
        //
        //                            public void run() {
        //                                imgView.setImageBitmap(bitmap);
        //                            }
        //                        });
        //                    }
        //                }).start();
        //            }
        //        });
        //
        }

    public Bitmap loadImageFromNetwork(String imageURL) {
        Bitmap result = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try {
            URL url = new URL(imageURL);
            Log.d(TAG, "download begining");
            Log.d(TAG, "download url:" + url);
            /* Open a connection to that URL. */
            URLConnection ucon = url.openConnection();

			/*
             * Define InputStreams to read from the URLConnection.
			 */
            is = ucon.getInputStream();
            bis = new BufferedInputStream(is);

            result = BitmapFactory.decodeStream(bis);
            Log.d("TAG", "download end");
        } catch (MalformedURLException e) {
            Log.e(TAG, "Bad ad URL", e);
        } catch (IOException e) {
            Log.d("ImageManager", "Error: " + e);
        } finally {
            try {
                if (is != null)
                    is.close();
                if (bis != null)
                    bis.close();
            } catch (IOException e) {
                Log.w(TAG, "Error closing stream.");
            }
        }
        return result;
    }

}