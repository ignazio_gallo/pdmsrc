package pdm.a14.usingtodolistprovider;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.Toolbar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class NewTodoItemActivity extends Activity {

    private static final String TAG = "NewTodoItemActivity";
    private static final int ACTION_TAKE_PHOTO = 1;
    // fields storing the data of the todoitem
    private GregorianCalendar creationDate = null;
    private GregorianCalendar dueDate = null;
    private boolean isDueDateRequired = false; // to recall if the user explicitly selected a due date or not
    private String task = null;
    private Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        creationDate = (GregorianCalendar) GregorianCalendar.getInstance();
        dueDate = new GregorianCalendar();
        dueDate.setTimeInMillis(creationDate.getTimeInMillis());

        setContentView(R.layout.activity_new_todo_item);


        // get reference to editText
        final EditText editText = (EditText) findViewById(R.id.new_todoitem_task);

        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) { // on entering edit text we deactivate all other GUI views
                    // get layout elements for duetime selection
                    findViewById(R.id.new_todoitem_dueDate).setEnabled(false);
                    findViewById(R.id.new_todoitem_dueTime).setEnabled(false);
                    findViewById(R.id.new_todoitem_duedaterequired).setEnabled(false);
                    findViewById(R.id.new_todoitem_takePhoto).setEnabled(false);
                    findViewById(R.id.new_todoitem_takeVideo).setEnabled(false);
                    findViewById(R.id.new_todoitem_done).setEnabled(false);
                    findViewById(R.id.new_todoitem_cancel).setEnabled(false);
                } else {  // on leaving edit text enable the other GUI components
                    findViewById(R.id.new_todoitem_duedaterequired).setEnabled(true);
                    if (isDueDateRequired) {
                        findViewById(R.id.new_todoitem_dueDate).setEnabled(true);
                        findViewById(R.id.new_todoitem_dueTime).setEnabled(true);
                    }
                    findViewById(R.id.new_todoitem_takePhoto).setEnabled(true);
                    findViewById(R.id.new_todoitem_takeVideo).setEnabled(true);
                    if (NewTodoItemActivity.this.task != null) // if the task is not empty enable the DONE button
                        findViewById(R.id.new_todoitem_done).setEnabled(true);
                    findViewById(R.id.new_todoitem_cancel).setEnabled(true);
                }
            }
        });


        // set the done action listener (we leave edit text pressing the done button on the keyboard)
        editText.setOnEditorActionListener(
                new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) { // android:imeOptions="actionDone"
                            // get the text of the task
                            String inserted_text = editText.getText().toString().trim();
                            if (inserted_text.length() != 0)  // if the inserted text is relevant
                                NewTodoItemActivity.this.task = inserted_text;
                            else
                                NewTodoItemActivity.this.task = null;
                            // edittext clear focus
                            editText.clearFocus();
                            // hide the softkey
                            InputMethodManager imm = (InputMethodManager) getSystemService(
                                    NewTodoItemActivity.this.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);
                        }
                        return true;
                    }
                }

        );

        // get layout elements for duetime selection
        final Button button_dueDate = (Button) findViewById(R.id.new_todoitem_dueDate);
        final Button button_dueTime = (Button) findViewById(R.id.new_todoitem_dueTime);
        final CheckBox cb_dueDateRequired = (CheckBox) findViewById(R.id.new_todoitem_duedaterequired);

        // set the listener for the due_date_required check box
        cb_dueDateRequired.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) { // enable the date selectors
                            button_dueDate.setEnabled(true);
                            button_dueTime.setEnabled(true);
                            isDueDateRequired = true;
                        } else {        // deactivate the data selectors
                            button_dueDate.setEnabled(false);
                            button_dueTime.setEnabled(false);
                            isDueDateRequired = false;
                        }

                    }
                }

        );


        // Build the date picker
        final DatePickerDialog datePicker = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        dueDate.set(GregorianCalendar.YEAR, year);
                        dueDate.set(GregorianCalendar.MONTH, monthOfYear);
                        dueDate.set(GregorianCalendar.DAY_OF_MONTH, dayOfMonth);
                        updateDueDateButton();
                        isDueDateRequired = true;
                    }
                }, dueDate.get(GregorianCalendar.YEAR), dueDate.get(GregorianCalendar.MONTH),
                dueDate.get(GregorianCalendar.DAY_OF_MONTH));

        //Define the listener for due_date button: show the date picker
        button_dueDate.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        datePicker.show();
                    }
                }

        );

        // build the time picker
        final TimePickerDialog timePicker = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        dueDate.set(GregorianCalendar.HOUR_OF_DAY, hourOfDay);
                        dueDate.set(GregorianCalendar.MINUTE, minute);
                        updateDueTimeButton();
                        isDueDateRequired = true;
                    }
                }, dueDate.get(GregorianCalendar.HOUR_OF_DAY),
                dueDate.get(GregorianCalendar.MINUTE), true);

        // define the listener for the due_time button
        button_dueTime.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        timePicker.show();
                    }
                }

        );

    }

    // Done button handler
    public void onDone(View v) {
        if (this.task != null) {
            // Build the bundle
            Bundle b = new Bundle();
            b.putString(TodoItem.NVP_KEY.TASK, task);
            b.putLong(TodoItem.NVP_KEY.CREATION_DATE, creationDate.getTimeInMillis());
            b.putBoolean(TodoItem.NVP_KEY.IS_DUE_DATE_REQUIRED, isDueDateRequired);
            if (isDueDateRequired)
                b.putLong(TodoItem.NVP_KEY.DUE_DATE, dueDate.getTimeInMillis());
            if (imageUri != null)
                b.putString(TodoItem.NVP_KEY.IMAGE_URI, imageUri.toString());

            // use setResult to pass the result to main activity
            Intent result = new Intent();
            result.putExtras(b);
            setResult(RESULT_OK, result);
            this.finish();
        }
    }

    // Cancel button handler
    public void onCancel(View v) {
        // use setResult to signal that no result is provided
        setResult(RESULT_CANCELED);
        this.finish();
    }


    // Add image button handler
    public void onAddPhoto(View v) {
        Log.i(TAG, "get photo");

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // check that there exists an application responding to the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, ACTION_TAKE_PHOTO);
        } else {
            Toast.makeText(getApplicationContext(), R.string.new_todoitem_camera_application_not_found,
                    Toast.LENGTH_SHORT).show();
            return;
        }
    }


    // Add video button handler
    public void onAddVideo(View v) {
        Toast.makeText(getApplicationContext(), "To be implemented", Toast
                .LENGTH_SHORT).show();
    }

    /**
     * Return the file for saving the specified camera media type (image or video)
     * or null if the specified media type is unknown or the file cannot be
     * created.
     */
    private String getOutputMediaFileName(int type) {
        if (type != MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE && //
                type != MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO) {

        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        switch (type) {
            case MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE:
                return "IMG_" + timeStamp + ".jpg";
            case MediaStore.Files.FileColumns.MEDIA_TYPE_VIDEO:
                return "VID_" + timeStamp + ".mp4";
            default: {
                Log.e(TAG, "Unkown media type: " + type);
                throw new ImplementationError("Unknown media type.");
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ACTION_TAKE_PHOTO && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap image = (Bitmap) extras.get("data");
            try {
                // get the approprite fileName
                String outputMediaFileName = getOutputMediaFileName(MediaStore.Files
                        .FileColumns
                        .MEDIA_TYPE_IMAGE);
                // open the file output stream in internal memory
                FileOutputStream fos = openFileOutput(outputMediaFileName, Context
                        .MODE_PRIVATE);
                // get the image as byte array
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                // write the bytes on the file output stream
                fos.write(stream.toByteArray());
                // the the File object with the speficied name in the internal memory dir
                File outputMediaFile = new File(this.getFilesDir(), outputMediaFileName);
                // get the uri
                imageUri = Uri.fromFile(outputMediaFile);
                Log.i(TAG, "Image saved to file " + imageUri.toString());

                ((ImageView) findViewById(R.id.new_todoitem_takePhoto)).setImageBitmap(image);
            } catch (IOException e) {
                Log.i(TAG, "Image cannot be saved to file " + e.toString());
                Toast.makeText(getApplicationContext(), R.string
                                .new_todoitem_image_file_cannot_be_created,
                        Toast.LENGTH_SHORT).show();
            }
        }
    }


    // updates the date in the DueDate Button

    private void updateDueDateButton() {
        ((Button) findViewById(R.id.new_todoitem_dueDate)).setText(String.format(String.format("%1$td/%1$tm/%1$tY", dueDate)));
    }

    // updates the time in the DueTime Button
    private void updateDueTimeButton() {
        ((Button) findViewById(R.id.new_todoitem_dueTime)).setText(String.format(String.format("%1$tH:%1$tM", dueDate)));
    }


    @Override
    protected void onResume() {
        super.onResume();

        // we update creation date when the activity get the foreground

        if (dueDate.equals(creationDate) || !isDueDateRequired) {//
            creationDate = (GregorianCalendar) GregorianCalendar.getInstance();
            dueDate = new GregorianCalendar();
            dueDate.setTimeInMillis(creationDate.getTimeInMillis());
        }
        updateDueDateButton();
        updateDueTimeButton();
    }


}
