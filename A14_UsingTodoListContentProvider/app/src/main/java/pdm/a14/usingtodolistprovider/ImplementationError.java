package pdm.a14.usingtodolistprovider;

class ImplementationError extends RuntimeException {

    public ImplementationError() {
        super("Implementation error!!");
    }

    public ImplementationError(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ImplementationError(String detailMessage) {
        super(detailMessage);
    }

    public ImplementationError(Throwable throwable) {
        super(throwable);
    }

}
