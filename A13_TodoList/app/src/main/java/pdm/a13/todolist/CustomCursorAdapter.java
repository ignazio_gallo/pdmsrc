package pdm.a13.todolist;

import java.util.GregorianCalendar;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import pdm.a13.todolist.contentprovider.TodoListContentProvider;
import pdm.a13.todolist.contentprovider.TodoListContentProviderContract;

/* Cusomized Adapter */
class CustomCursorAdapter extends CursorAdapter {

	private static String TAG = "CustomCursorAdapter";

	private LayoutInflater inflater;
	private Drawable defaultImage;
    private Context context;

	public CustomCursorAdapter(Context context, Cursor cursor) {
        super(context.getApplicationContext(), cursor, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        this.context = context.getApplicationContext();
		this.inflater = LayoutInflater.from(context);
		defaultImage = context.getDrawable(android.R.drawable.ic_menu_camera);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
        long idx = cursor.getLong(cursor.getColumnIndex(TodoListContentProviderContract.TodoItems
                ._ID));
        String task = cursor.getString(cursor.getColumnIndex(TodoListContentProviderContract
                .TodoItems
                .COLUMN_NAME_TASK));
        GregorianCalendar creationDate = new GregorianCalendar();
        creationDate.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(TodoListContentProviderContract.TodoItems
                .COLUMN_NAME_CREATION_DATE)));
        boolean isDueDateDefined = cursor.getInt(cursor.getColumnIndex
                (TodoListContentProviderContract.TodoItems.COLUMN_NAME_IS_DUEDATE_REQUIRED)) == 1;
        GregorianCalendar dueDate = null;
        if (isDueDateDefined) {
            dueDate = new GregorianCalendar();
            dueDate.setTimeInMillis(cursor.getLong(cursor.getColumnIndex(TodoListContentProviderContract.TodoItems
                    .COLUMN_NAME_DUE_DATE)));
        }

        // get the image Uri
        String uriString = cursor.getString(cursor
                .getColumnIndex(TodoListContentProviderContract.TodoItems
                        .COLUMN_NAME_URI_IMAGE));
        byte[] bytes = cursor.getBlob(cursor.getColumnIndex(TodoListContentProviderContract
                .TodoItems.COLUMN_NAME_IMAGE));
        Log.d(TAG,"bytes " + (bytes == null ? "null" : bytes.length));
        Uri uriImage = null;
        if (uriString != null)
            uriImage = Uri.parse(uriString);

        // build the TodoItem instance
        TodoItem item = new TodoItem(context, idx, task, creationDate, isDueDateDefined,
                dueDate,uriImage);

        TextView textView = (TextView) view.findViewById(R.id.todolist_item_info);
        ImageView imageView = (ImageView) view.findViewById(R.id.todolist_item_image);

		if (item.getImage() != null)
			imageView.setImageBitmap(item.getImage());
		else
			imageView.setImageDrawable(defaultImage);
        ((TextView) view.findViewById(R.id.todolist_item_info)).setText(item.toString());
	}

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(R.layout.todo_listview_item, parent, false);
    }
}
