package pdm.a13.todolist;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.ArrayList;

import pdm.a13.todolist.contentprovider.TodoListContentProvider;

public class MainActivity extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "MainActivity";
    private static final int SUBACTIVITY_NEW_TODO_ITEM = 1;
    private CustomCursorAdapter cursorAdapter;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // set content layout
        setContentView(R.layout.activity_main);
        // set the action bar
        Toolbar tb = (Toolbar) findViewById(R.id.main_activity_toolbar);
        this.setActionBar(tb);

        // create cursorAdapter
        cursorAdapter = new CustomCursorAdapter(this,null);
        // get the references to the views
        final ListView listView = (ListView) findViewById(R.id.list_view);
        // set the listview adapter
        listView.setAdapter(cursorAdapter);

        // init the loader
        getLoaderManager().initLoader(MAIN_LOADER_ID, null, this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v(TAG, "onResume()");
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.v(TAG, "onPause()");
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    /*
     * Menu actions
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_item: { // start sub-activity for result
                Intent intent = new Intent(this.getApplicationContext(), NewTodoItemActivity.class); // Explicit intent creation
                startActivityForResult(intent, SUBACTIVITY_NEW_TODO_ITEM); // Start as sub-activity for result
                return true;
            }
            case R.id.action_delete_DB: {
                // 1. Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                // 2. Chain together various setter methods to set the dialog characteristics
                builder.setMessage(R.string.dialog_cleardb_database_confirm);
                // Add the buttons
                builder.setPositiveButton(R.string.dialog_cleardb_delete, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        getContentResolver().delete(TodoListContentProvider.CONTENT_URI,null,null);
                    }
                });

                builder.setNegativeButton(R.string.dialog_cleardb_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                // 3. Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();
                return true;
            }
            case R.id.action_settings:
                showNonImplemented();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case SUBACTIVITY_NEW_TODO_ITEM:
                switch (resultCode) {
                    case Activity.RESULT_OK: // add the new todoItem
                        TodoItem item = TodoItem.buildFrom(this, data.getExtras());
                        Uri result = getContentResolver().insert(TodoListContentProvider.CONTENT_URI,
                                item.getAsContentValue());
                        long ID = ContentUris.parseId(result); // extract the ID from the result uri
                        item.setID(ID); // set the ID of the inserted element
                        return;
                    case Activity.RESULT_CANCELED: // nothing to do
                        return;
                    default:
                        throw new ImplementationError();
                }
            default:
                throw new ImplementationError();
        }
    }


    private void showNonImplemented() {
        String msg = "Not implemented yet!";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }


    /*
	 * LoaderCallbacks IMPLEMENTATION
	 */
    private static final int MAIN_LOADER_ID = 0;

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case MAIN_LOADER_ID:
                CursorLoader loader = new CursorLoader(this, TodoListContentProvider.CONTENT_URI,
                        null, null,
                        null, null);
                return loader;
            default:
                throw new ImplementationError();
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        Log.d(TAG, "onLoadFinished: elements in the cursor " + (cursor == null ? " null " +
                "cursor " : cursor.getCount()));
        cursorAdapter.swapCursor(cursor);
        if (cursor == null || cursor.getCount() == 0) {
            String msg = getResources().getString(R.string.new_todoitem_listIsEmpty);
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> arg0) {
        cursorAdapter.swapCursor(null);
    }
}


