package pdm.a13.todolist.contentprovider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import pdm.a13.todolist.NewTodoItemActivity;
import pdm.a13.todolist.R;
import pdm.a13.todolist.database.DBContract;
import pdm.a13.todolist.database.DBOpenHelper;

public class TodoListContentProvider extends ContentProvider {

    private final static String TAG = "TodoListContentProvider";

    /**
     * Content Uri.
     */
    public static final Uri CONTENT_URI = TodoListContentProviderContract.CONTENT_URI;

    private static final int ALLROWS = 1;
    private static final int SINGLE_ROW = 2;
    private static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("pdm.todolist.provider", "todoitems", ALLROWS);
        uriMatcher.addURI("pdm.todolist.provider", "todoitems/#", SINGLE_ROW);
    }

    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case ALLROWS:
                return "vnd.android.cursor.dir/vnd.pdm.todolist.provider.todoitems";
            case SINGLE_ROW:
                return "vnd.android.cursor.item/vnd.pdm.todolist.provider.todoitems";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    private DBOpenHelper dbHelper; // reference to the OpenHelper

    @Override
    public boolean onCreate() {
        // Construct the underlying database.
        // Defer opening the database until you need to perform a query or transaction.
        this.dbHelper = DBOpenHelper.getInstance(this.getContext());
        return true;
    }

    //
    @Override
    public CustomCursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                              String sortOrder) {

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setTables(DBContract.TodoItems.TABLE_NAME);

        // If this is a row query, limit the result set to the passed in row.
        switch (uriMatcher.match(uri)) {
            case SINGLE_ROW: {
                String rowID = uri.getPathSegments().get(1);
                queryBuilder.appendWhere(DBContract.TodoItems._ID + "=" + rowID);
                break;
            }
            default:
                break;
        }
        // get the cursor from the DB
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null,
                sortOrder);
        CustomCursor customCursor = new CustomCursor(cursor);
        customCursor.setNotificationUri(getContext().getContentResolver(), uri);
        return customCursor;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        // Open a read / write database to support the transaction.
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // If this is a row URI, limit the deletion to the specified row.
        switch (uriMatcher.match(uri)) {
            case SINGLE_ROW: {
                String rowID = uri.getPathSegments().get(1);
                selection = DBContract.TodoItems._ID + " = " + rowID
                        + (!TextUtils.isEmpty(selection) ? " AND (" + selection + ")" : "");
                break;
            }
            default:
                break;
        }
        // To return the number of deleted items, you must specify a where  clause.
        // To delete all rows and return a value, pass in “1”.
        if (selection == null)
            selection = "1";

        Log.i(TAG, selection);
        // Execute the deletion.
        int deleteCount = db.delete(DBContract.TodoItems.TABLE_NAME, selection, selectionArgs);
        // Notify any observers of the change in the data set.
        getContext().getContentResolver().notifyChange(uri, null);
        return deleteCount;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // Create the ContentValue for the DB from the content value for the content provider
        ContentValues dbcv = new ContentValues();
        // copy values from values to dbcv
        dbcv.put(DBContract.TodoItems.COLUMN_NAME_TASK, values.getAsString
                (TodoListContentProviderContract.TodoItems.COLUMN_NAME_TASK));
        dbcv.put(DBContract.TodoItems.COLUMN_NAME_IS_DUEDATE_REQUIRED, values.getAsBoolean
                (TodoListContentProviderContract.TodoItems.COLUMN_NAME_IS_DUEDATE_REQUIRED));
        dbcv.put(DBContract.TodoItems.COLUMN_NAME_CREATION_DATE, values.getAsLong
                (TodoListContentProviderContract.TodoItems.COLUMN_NAME_CREATION_DATE));
        dbcv.put(DBContract.TodoItems.COLUMN_NAME_DUE_DATE, values.getAsLong
                (TodoListContentProviderContract.TodoItems.COLUMN_NAME_DUE_DATE));
        // if there is an image extract the image, saves it in the local mem,
        // create the uri and add the uri to the content value
        Uri imageUri = null;
        byte[] imageBytes = values.getAsByteArray(TodoListContentProviderContract.TodoItems
                .COLUMN_NAME_IMAGE);
        if (imageBytes != null) {
            try {
                // save il local file
                String outputMediaFileName = NewTodoItemActivity.getOutputMediaFileName(MediaStore.Files
                        .FileColumns
                        .MEDIA_TYPE_IMAGE);
                // open the file output stream in internal memory
                FileOutputStream fos = getContext().openFileOutput(outputMediaFileName, Context
                        .MODE_PRIVATE);
                // get the image as byte array
                fos.write(imageBytes);
                // the the File object with the speficied name in the internal memory dir
                File outputMediaFile = new File(getContext().getFilesDir(), outputMediaFileName);
                // get the uri
                imageUri = Uri.fromFile(outputMediaFile);
            } catch (IOException e) {
                Log.i(TAG, "Image cannot be saved to file " + e.toString());
                Toast.makeText(getContext(), R.string
                                .new_todoitem_image_file_cannot_be_created,
                        Toast.LENGTH_SHORT).show();
            }
        }
        if (imageUri != null)
            dbcv.put(TodoListContentProviderContract.TodoItems.COLUMN_NAME_URI_IMAGE, imageUri
                    .toString());


        // Open a read / write database to support the transaction.
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Insert the values into the table
        long id = db.insert(DBContract.TodoItems.TABLE_NAME, null, dbcv);
        if (id > -1) {
            // Construct and return the URI of the newly inserted row.
            Uri insertedId = ContentUris.withAppendedId(CONTENT_URI, id);
            // Notify any observers of the change in the data set.
            getContext().getContentResolver().notifyChange(insertedId, null);
            return insertedId;
        } else
            return null;
    }

    // @HINT: TO BE IMPLEMENTED
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // TODOint count;
        return 0;
    }

}
