package pdm.a19.fragmentexample;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;

/**
 * Created by ferram on 09/05/16.
 */
public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_layout);

        String layoutTag = (String) findViewById(R.id.main_activity_layout).getTag();
        // Check that the activity is using the large layout version
        if (layoutTag.equals("large_screen")) {
            // disable buttons
            findViewById(R.id.button1).setEnabled(false);
            findViewById(R.id.button2).setEnabled(false);
        } else {
            Fragment fr = new FragmentOne();
            FragmentManager fm = getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.add(R.id.fragment_container, fr);
            fragmentTransaction.commit();
        }
    }

    // onClick for button1 and button2
    public void selectFrag(View view) {
        if (view == findViewById(R.id.button2)) {
            // replace FragmentOne with FragmentTwo
            FragmentManager fm = getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, new FragmentTwo());
            fragmentTransaction.commit();

        } else {
            // replace FragmentTwo with FragmentOne
            FragmentManager fm = getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, new FragmentOne());
            fragmentTransaction.commit();
        }
    }


}
