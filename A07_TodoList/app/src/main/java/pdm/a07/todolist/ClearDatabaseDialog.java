package pdm.a07.todolist;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

/**
 * Created by ferram on 07/04/16.
 */
public class ClearDatabaseDialog extends DialogFragment {

    private DBAdapter dbAdapter;
    private ArrayList<TodoItem> todoItems;  // the list containing the items
    private ArrayAdapter<TodoItem> listViewAdapter; // the adapter populating the listview

    public ClearDatabaseDialog(){}


    public ClearDatabaseDialog(DBAdapter dbAdapter, ArrayAdapter<TodoItem> listViewAdapter, ArrayList<TodoItem> todoItems) {
        this.dbAdapter = dbAdapter;
        this.listViewAdapter = listViewAdapter;
        this.todoItems = todoItems;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // set dialog message
        builder.setMessage(R.string.dialog_cleardb_database_confirm);
        // Add positive button
        builder.setPositiveButton(R.string.dialog_cleardb_delete, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dbAdapter.deleteTodoItemsTable();
                todoItems.clear();
                listViewAdapter.notifyDataSetChanged();
            }
        });
        // Add negative button
        builder.setNegativeButton(R.string.dialog_cleardb_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }
}
