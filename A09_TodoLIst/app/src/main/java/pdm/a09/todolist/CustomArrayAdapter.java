package pdm.a09.todolist;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/* Cusomized Adapter */
class CustomArrayAdapter extends ArrayAdapter<TodoItem> {

	private static String TAG = "CustomArrayAdapter";

	private LayoutInflater inflater;
	private Drawable defaultImage;

	public CustomArrayAdapter(Context context, ArrayList<TodoItem> todoItems) {
		super(context.getApplicationContext(), R.layout.todo_listview_item, todoItems);
		this.inflater = LayoutInflater.from(context);
		//this.todoItems = todoItems;
		defaultImage = context.getDrawable(android.R.drawable.ic_menu_camera);
	}

	@Override
	public View getView(int pos, View convertView, ViewGroup parent) {
		if (convertView == null) // inflate a new view if this is not an update.
			convertView = inflater.inflate(R.layout.todo_listview_item, parent, false);
		ImageView imageView = (ImageView) convertView.findViewById(R.id.todolist_item_image);
		TodoItem item = super.getItem(pos); // get the todoItem
		if (item.getImage() != null)
			imageView.setImageBitmap(item.getImage());
		else
			imageView.setImageDrawable(defaultImage);
        ((TextView) convertView.findViewById(R.id.todolist_item_info)).setText(item.toString());
		return convertView;
	}
}
