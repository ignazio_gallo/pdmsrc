package pdm.a19.androidlocation;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;


public class MainActivity extends Activity {

    private static final String TAG = "MainActivity";

    private Location proximityAlertLocation;
    private LocationManager locationManager;
    private Location location;
    private MyLocationListener myLocationListener;
    private Criteria criteria;
    private Boolean proximityAlertAdded, locationUpdatesRequest;
    private final float RADIUS = 100;
    private int selectedPowerId;
    private MyAddress myAddress;
    private Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        proximityAlertAdded = false;
        locationUpdatesRequest = false;
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_COARSE);

        final RadioGroup radioGroupPower = (RadioGroup) findViewById(R.id.rdgp_power);
        myAddress = new MyAddress();
        final CheckBox accuracyChoice = (CheckBox) findViewById(R.id.accuracy);
        ((Button) findViewById(R.id.btn_choose)).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View v) {
                if (accuracyChoice.isChecked()) {
                    criteria.setAccuracy(Criteria.ACCURACY_FINE);
                    ((TextView) findViewById(R.id.lblAccuracy)).setText(R.string
                            .accuracy_fine);
                } else {
                    criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                    accuracyChoice.setText(R.string.accuracy_coarse);
                }
                selectedPowerId = radioGroupPower.getCheckedRadioButtonId();
                RadioButton radioPower = (RadioButton) findViewById(selectedPowerId);
                TextView power = (TextView) findViewById(R.id.lblPower);
                switch (radioPower.getId()) {
                    case R.id.radioButton:
                        criteria.setPowerRequirement(Criteria.POWER_HIGH);
                        power.setText(R.string.power_high);
                        break;
                    case R.id.radioButton2:
                        criteria.setPowerRequirement(Criteria.POWER_MEDIUM);
                        power.setText(R.string.power_medium);
                        break;
                    case R.id.radioButton3:
                        criteria.setPowerRequirement(Criteria.POWER_LOW);
                        power.setText(R.string.power_low);
                        break;
                }
            }
        });

        // proximity alert button
        ((Button) findViewById(R.id.btnProxAlert)).setOnClickListener(new View
                .OnClickListener() {
            @Override
            public void onClick(View v) {
                if (location != null) {
                    addRemoveProximityAlert();
                } else {
                    Toast.makeText(getApplicationContext(), "prima ottieni una location", Toast.LENGTH_LONG).show();
                }
            }
        });

        // update location button
        ((Button) findViewById(R.id.btnUpdate)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStopLocationUpdates();
            }
        });

        criteria.setCostAllowed(false);
    }

    private class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            ((TextView) findViewById(R.id.lblLatitude)).setText("Lat: " + String.valueOf
                    (location.getLatitude()));
            ((TextView) findViewById(R.id.lblLongitude)).setText("Long: " + String
                    .valueOf(location.getLongitude()));
            String provider = locationManager.getBestProvider(criteria, true);
            ((TextView) findViewById(R.id.lblProvider)).setText("Provider: " + provider);
            Toast.makeText(MainActivity.this, "location changed", Toast.LENGTH_LONG).show();
            printMyAddress(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    }

    private static final String PROX_ALERT_INTENT = "pdm.a19.androidlocation.PROXIMITY_ALERT";

    public void createProximityAlert(double latitude, double longitude) {
        locationPermissionManagment();
        if (isLocationPermissionGranted()) {
            try {
                Intent intent = new Intent(PROX_ALERT_INTENT);
                // add proximity aler info
                intent.putExtra(ProximityReceiver.KEY_NVP.LATITUDE, location.getLatitude());
                intent.putExtra(ProximityReceiver.KEY_NVP.RADIUS, RADIUS);
                // build pending intent
                PendingIntent proximityIntent =
                        PendingIntent.getBroadcast(getBaseContext(), 0, intent,
                                PendingIntent.FLAG_CANCEL_CURRENT);

                locationManager.addProximityAlert(latitude, longitude, RADIUS, -1, proximityIntent);
                IntentFilter filter = new IntentFilter(PROX_ALERT_INTENT);
                registerReceiver(new ProximityReceiver(), filter);
                ((TextView) findViewById(R.id.proxText)).setText("Proximity Alert at: " +
                        "\nLat: " + location.getLatitude() + " " +
                        "Long: " + location.getLongitude());


            } catch (SecurityException e) {
                // nothing to do, it will never be executed since
                // we already checked for permission
            }

        }
    }

    public void removeProximityAlert() {
        Intent intent = new Intent(PROX_ALERT_INTENT);
        PendingIntent proximityIntent = PendingIntent.getBroadcast(getBaseContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        try {
            locationManager.removeProximityAlert(proximityIntent);
            ((TextView) findViewById(R.id.proxText)).setText("");
        } catch (SecurityException e) {
        }
    }

    private void addRemoveProximityAlert() {
        if (!proximityAlertAdded) {
            //addProximityAlert(Constants.GORD40_LATITUDE, Constants.GORD40_LONGITUDE);
            createProximityAlert(location.getLatitude(), location.getLongitude());
            ((Button) findViewById(R.id.btnProxAlert)).setText(R.string.btn_alert_off);
            proximityAlertAdded = true;

        } else {
            removeProximityAlert();
            ((Button) findViewById(R.id.btnProxAlert)).setText(R.string.btn_alert_on);
            proximityAlertAdded = false;
        }
    }

    private void startStopLocationUpdates() {
        if (!locationUpdatesRequest) {
            // Verify that all required contact permissions have been granted.
            locationPermissionManagment();
            try {
                if (isLocationPermissionGranted()) {
                    String provider = locationManager.getBestProvider(criteria, false);

                    location = locationManager.getLastKnownLocation(provider);

                    myLocationListener = new MyLocationListener();
                    if (location != null) {
                        myLocationListener.onLocationChanged(location);
                    } else {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(intent);
                    }
                    locationManager.requestLocationUpdates(provider, 200, 1, myLocationListener);
                    ((Button) findViewById(R.id.btnUpdate)).setText(getString(R
                            .string.btn_stop_update));
                    locationUpdatesRequest = true;
                } else {
                    Toast.makeText(getApplicationContext(), "Location provider is not available.",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (SecurityException e) {
                // nothing to do, it will never be executed since
                // we already checked for permission
            }
        } else {
            ((Button) findViewById(R.id.btnUpdate)).setText(getString(R.string.btn_start_update));
            locationUpdatesRequest = false;
            stopLocationUpdates();
        }
    }

    private boolean isLocationPermissionGranted() {
        return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void locationPermissionManagment() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Contacts permissions have not been granted.
            Log.i(TAG, "Location permissions has NOT been granted. Requesting permissions.");
            requestLocationPermissions();
        } else {
            // Contact permissions have been granted. Show the contacts fragment.
            Log.i(TAG,
                    "Contact permissions have already been granted. Displaying contact details.");
        }
    }


    protected void stopLocationUpdates() {
        locationPermissionManagment();
        try {
            if (isLocationPermissionGranted())
                locationManager.removeUpdates(myLocationListener);
        } catch (SecurityException e) {
            // nothing to do, we already checked for permission
        }

    }

    public void printMyAddress(Location location) {
        geocoder = new Geocoder(this, Locale.getDefault());
        ((TextView) findViewById(R.id.address)).setText(myAddress.getMyLocationAddress(location,
                geocoder));
    }


    // Id to identify a location permission request.
    private static final int REQUEST_PERMISSION_LOCATION = 1;


    // Location permissions required
    private static String[] PERMISSIONS_LOCATION = {Manifest.permission.ACCESS_FINE_LOCATION};

    /**
     * Requests the Location permissions.
     * If the permission has been denied previously, an AlertDialog will prompt the user to grant
     * the permission, otherwise it is requested directly.
     */
    private void requestLocationPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            Log.i(TAG,
                    "Displaying location permission rationale to provide additional " +
                            "context.");

            // Build AlertDialog
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.permission_locations_rationale);
            builder.setPositiveButton("Require permission",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    PERMISSIONS_LOCATION,
                                    REQUEST_PERMISSION_LOCATION);
                        }
                    });

            builder.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            // Contact permissions have not been granted yet. Request them directly.
            ActivityCompat.requestPermissions(this, PERMISSIONS_LOCATION,
                    REQUEST_PERMISSION_LOCATION);
        }
    }


    //Callback received when a permissions request has been completed
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {

        if (requestCode == REQUEST_PERMISSION_LOCATION) {
            Log.i(TAG, "Received response for contact permissions request.");

            // We have requested multiple permissions for contacts, so all of them need to be
            // checked.
            if (verifyPermissions(grantResults)) {
                // All required permissions have been granted, display contacts fragment.
                Toast.makeText(getApplicationContext(), R.string
                                .permision_available_location,
                        Toast.LENGTH_SHORT).show();
            } else {
                Log.i(TAG, "Location permissions were NOT granted.");
                Toast.makeText(getApplicationContext(),
                        R.string.permissions_not_granted,
                        Toast.LENGTH_SHORT).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    /**
     * Check that all given permissions have been granted by verifying that each entry in the
     * given array is of the value {@link PackageManager#PERMISSION_GRANTED}.
     *
     * @see Activity#onRequestPermissionsResult(int, String[], int[])
     */
    public static boolean verifyPermissions(int[] grantResults) {
        // At least one result must be checked.
        if (grantResults.length < 1) {
            return false;
        }

        // Verify that each required permission has been granted, otherwise return false.
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


}

