package pdm.a19.androidlocation;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import java.io.IOException;
import java.util.List;

public class MyAddress {

    StringBuilder strAddress;
    String stringAddress;

    public String getMyLocationAddress(Location location, Geocoder geocoder) {
        try {
            List<Address> addressList = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            Address fetchedAddresses = addressList.get(0);
            strAddress = new StringBuilder();
            for (int i = 0; i < fetchedAddresses.getMaxAddressLineIndex(); i++) {
                strAddress.append(fetchedAddresses.getAddressLine(i)).append("\n");
            }
            stringAddress = strAddress.toString();
            Log.d("STREET ADDRESS", stringAddress);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringAddress;
    }
}
