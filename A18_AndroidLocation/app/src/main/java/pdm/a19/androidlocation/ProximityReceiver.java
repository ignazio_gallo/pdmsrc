package pdm.a19.androidlocation;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

public class ProximityReceiver extends BroadcastReceiver {

    public static class KEY_NVP {
        public static String LATITUDE = "latitude";
        public static String LONGITUDE = "longitude";
        public static String RADIUS = "radius";
    }

    private static final String TAG = "ProximityReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG,"onReceive");
        String s = LocationManager.KEY_PROXIMITY_ENTERING;
        // extract state
        Boolean state = intent.getBooleanExtra(s, false);
        // extract location alert info
        double latitude = intent.getDoubleExtra(KEY_NVP.LATITUDE, 0);
        double longitude = intent.getDoubleExtra(KEY_NVP.LONGITUDE, 0);
        double radius = intent.getFloatExtra(KEY_NVP.RADIUS, 0);

        PendingIntent.getBroadcast(context, 0, intent, 0);

        if (state) {
            Toast.makeText(context, "You are in your interest area (lat:" + latitude
                            + ", long:" + longitude + ", radius<=" + radius + "m)",
                    Toast.LENGTH_LONG)
                    .show();
            Log.d(TAG, "entering interest area");
        } else {
            Toast.makeText(context, "You are out of your interest area (lat:" + latitude
                            + ", long:" + longitude + ", radius>" + radius + "m)",
                    Toast.LENGTH_LONG)
                    .show();
            Log.d(TAG, "leaving interest area");
        }
    }
}
