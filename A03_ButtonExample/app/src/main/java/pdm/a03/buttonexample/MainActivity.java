package pdm.a03.buttonexample;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "MainActivity";

    private int counter = 0;
    private TextView output;
    private String counterString;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate()");

        setContentView(R.layout.activity_main);

        // get couter string
        counterString = getResources().getString(R.string.counter);


        // we get the view references
        output = (TextView) findViewById(R.id.output);
        final Button button_plus = (Button) findViewById(R.id.button_plus);
        final Button button_minus = (Button) findViewById(R.id.button_minus);
        final Button button_reset = (Button) findViewById(R.id.button_reset);

        // register click event listeners
        button_plus.setOnClickListener(this);
        button_minus.setOnClickListener(this);
        button_reset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String clickOn = "";
        if (v.getId() == R.id.button_plus) {//click on button +1
            clickOn = "+1";
            counter++;
        }

        if (v.getId() == R.id.button_minus) {//click on button -1
            clickOn = "-1";
            counter--;
        }

        if (v.getId() == R.id.button_reset) {//click on button reset
            clickOn = "reset";
            counter = 0;
        }

        Log.d(TAG, "Click on " + clickOn + "button, counter=" + counter);
        output.setText(String.format(counterString, counter));
    }


}