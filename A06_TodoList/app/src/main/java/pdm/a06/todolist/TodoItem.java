package pdm.a06.todolist;

import android.content.ContentValues;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * An element of the TodoList.
 */
public class TodoItem {

    private long _ID;
    private String task;  // il task
    private GregorianCalendar createOn; // la data di creazione del task

    // Used to construct a new TodoItem
    public TodoItem(String task) {
        super();
        this.task = task;
        this.createOn = new GregorianCalendar();
    }

    // Used to construct a TodoItem from a DB record
    public TodoItem(long id, String task, long creationDate) {
        super();
        this._ID = id;
        this.task = task;
        this.createOn = new GregorianCalendar();
        this.createOn.setTimeInMillis(creationDate);
    }

    public String getTask() {
        return task;
    }

    public GregorianCalendar getCreationDate(){
        return createOn;
    }

    /**
     * @return the content values representing this item.
     */
    public ContentValues getAsContentValue() {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.TodoItems.COLUMN_NAME_TASK, this.task);
        cv.put(DBContract.TodoItems.COLUMN_NAME_CREATION_DATE, this.createOn.getTimeInMillis());
        return cv;
    }


    public void setID(long id){
        this._ID = id;
    }

    public long getID(){
        return this._ID;
    }


    @Override
    public String toString() {
        String strCreteOn = new SimpleDateFormat("dd-MMM-yyyy").format(createOn.getTime());
        return "idx= " + this._ID + " - " + strCreteOn + ": " + this.task;
    }


}
