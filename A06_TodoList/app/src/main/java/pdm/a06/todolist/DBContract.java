package pdm.a06.todolist;

import android.provider.BaseColumns;

/** DB CONTRACT */
public class DBContract {
	
	// DATABASE 
	static final String DATABASE_NAME = "todolist.db";
	static final int DATABASE_VERSION = 1;


	// To prevent someone from accidentally instantiating the contract class,
	// give it an empty constructor.
	private DBContract() {
	}

	/* Inner class that defines the table contents */
	static abstract class TodoItems implements BaseColumns {

		static final String TABLE_NAME = "todoItems";
		static final String COLUMN_NAME_TASK = "task";
		static final String COLUMN_NAME_CREATION_DATE = "creation_date";

	}
}
